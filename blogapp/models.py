from django.db import models


class Blog(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    image = models.ImageField(upload_to='blogimages')
    author = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Notice(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='notices')

    def __str__(self):
        return self.title


class Message(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField()
    subject = models.CharField(max_length=200)
    message = models.TextField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    title = models.CharField(max_length=200)
    about = models.TextField()
    image = models.ImageField(upload_to='categories')

    def __str__(self):
        return self.title


class Book(models.Model):
    name = models.CharField(max_length=200)
    # category = models.OneToOneField(Category, on_delete=models.CASCADE)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="books")
    # category = models.ManyToManyField(Category)
    about = models.TextField()
    writer = models.CharField(max_length=200)
    image = models.ImageField(upload_to='books')

    def __str__(self):
        return self.name + " (" + self.writer + ")"
