from django.contrib import admin
from .models import *


admin.site.register([Blog, Notice, Message, Category, Book])
