from django.urls import path
from .views import *

app_name = "blogapp"
urlpatterns = [
    path('', ClientHomeView.as_view(), name="home"),
    path('contact/', ClientContactView.as_view(), name="contact"),
    path('about/', ClientAboutView.as_view(), name="about"),
    path('sunil/', ClientSunilView.as_view(), name='sunil'),
    path('dipak/', ClientDipakView.as_view(), name='dipak'),
    path('cosmos/', ClientCosmosView.as_view(), name='cosmos'),
    path('blogs/<int:pk>/', ClientBlogDetailView.as_view(), name='blogdetail'),
    path('notices/', ClientNoticeListView.as_view(), name='noticelist'),
    path('notices/<int:pk>/', ClientNoticeDetailView.as_view(),
         name='noticedetail'),



    path('categories/<int:pk>/', ClientCategoryDetailView.as_view(),
         name='categorydetail'),
    path('books/<int:pk>/', ClientBookDetailView.as_view(),
         name='bookdetail'),



    path('blog-admin/', AdminHomeView.as_view(), name='adminhome'),
    path('blog-admin/login/', AdminLoginView.as_view(), name='adminlogin'),
    path('blog-admin/logout/', AdminLogoutView.as_view(), name='adminlogout'),


    path('blog-admin/messgaes/', AdminMessageListView.as_view(),
         name='adminmessagelist'),
    path('blog-admin/messages/<int:pk>/', AdminMessageDetailView.as_view(),
         name='adminmessagedetail'),


    # blog crud
    path("blog-admin/blogs/",
         AdminBlogListView.as_view(), name='adminbloglist'),
    path("blog-admin/blogs/create/",
         AdminBlogCreateView.as_view(), name='adminblogcreate'),
    path("blog-admin/blogs/<int:pk>/update/",
         AdminBlogUpdateView.as_view(), name='adminblogupdate'),
    path("blog-admin/blogs/<int:pk>/delete/",
         AdminBlogDeleteView.as_view(), name='adminblogdelete'),


    # notice crud
    path('blog-admin/notices/', AdminNoticeListView.as_view(),
         name='adminnoticelist'),
    path("blog-admin/notice/create/",
         AdminNoticeCreateView.as_view(), name='adminnoticecreate'),
    path("blog-admin/notices/<int:pk>/update/",
         AdminNoticeUpdateView.as_view(), name='adminnoticeupdate'),
    path("blog-admin/notices/<int:pk>/delete/",
         AdminNoticeDeleteView.as_view(), name='adminnoticedelete'),



    path('blog-admin/books/', AdminBookListView.as_view(),
         name='adminbooklist'),
    path('blog-admin/categories/', AdminCategoryListView.as_view(),
         name='admincategorylist'),

]
