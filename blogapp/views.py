from django.views.generic import *
from .models import *
from .forms import *
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin


class AdminLogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/blog-admin/login/')


class AdminLoginView(FormView):
    template_name = 'admintemplates/adminlogin.html'
    form_class = AdminLoginForm
    success_url = '/blog-admin/'

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']
        user = authenticate(username=uname, password=pword)
        if user is not None:
            login(self.request, user)

        else:
            return render(self.request, self.template_name, {
                'form': self.form_class,
                'error': 'Invalid username or password..\
                 please correct it and try again later.'})
        return super().form_valid(form)


class AdminHomeView(LoginRequiredMixin, TemplateView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/adminhome.html'

# blog


class AdminBlogListView(LoginRequiredMixin, ListView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/adminbloglist.html'
    model = Blog
    context_object_name = 'bloglist'


class AdminBlogCreateView(LoginRequiredMixin, CreateView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/adminblogcreate.html'
    form_class = BlogForm
    success_url = '/blog-admin/blogs/'


class AdminBlogUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/adminblogcreate.html'
    model = Blog
    form_class = BlogForm
    success_url = '/blog-admin/blogs/'


class AdminBlogDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/adminblogdelete.html'
    model = Blog
    success_url = '/blog-admin/blogs/'


class AdminNoticeCreateView(LoginRequiredMixin, CreateView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/adminnoticecreate.html'
    form_class = NoticeForm
    success_url = '/noticelist/'


class AdminNoticeUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminnoticecreate.html'
    model = Notice
    form_class = NoticeForm
    success_url = '/'


class AdminNoticeDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'admintemplates/adminnoticedelete.html'
    model = Notice
    success_url = '/notices/'


class AdminMessageListView(LoginRequiredMixin, ListView):
    template_name = "admintemplates/adminmessagelist.html"
    queryset = Message.objects.all().order_by('-id')
    context_object_name = "messagelist"


class AdminMessageDetailView(LoginRequiredMixin, DetailView):
    template_name = "admintemplates/adminmessagedetail.html"
    queryset = Message.objects.all().order_by('-id')
    context_object_name = "message"


class AdminNoticeListView(LoginRequiredMixin, ListView):
    template_name = 'admintemplates/adminnoticelist.html'
    model = Notice
    context_object_name = 'noticelist'


class AdminCategoryListView(LoginRequiredMixin, ListView):
    template_name = "admintemplates/admincategorylist.html"
    queryset = Message.objects.all().order_by('-id')
    context_object_name = "admincategorylist"


class AdminBookListView(LoginRequiredMixin, ListView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/adminbooklist.html'
    model = Book
    context_object_name = 'adminbooklist'


class AdminCategoryListView(LoginRequiredMixin, ListView):
    login_url = '/blog-admin/login/'
    template_name = 'admintemplates/admincategorylist.html'
    model = Category
    context_object_name = 'admincategorylist'


class ClientHomeView(ListView):
    template_name = "clienttemplates/clienthome.html"
    # model = Blog
    queryset = Blog.objects.all().order_by('-id')
    context_object_name = "bloglist"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['notices'] = Notice.objects.all().order_by('-id')
        context['messages'] = Message.objects.all().order_by('-id')
        context['books'] = Book.objects.all().order_by('-id')
        context['categories'] = Category.objects.all().order_by('-id')
        return context


class ClientCategoryDetailView(DetailView):
    template_name = 'clienttemplates/clientcategorydetail.html'
    model = Category
    context_object_name = 'category'


class ClientBookDetailView(DetailView):
    template_name = 'clienttemplates/clientbookdetail.html'
    model = Book
    context_object_name = 'book'


class ClientBlogDetailView(DetailView):
    template_name = 'clienttemplates/clientblogdetail.html'
    model = Blog
    context_object_name = 'blog'


class ClientContactView(CreateView):
    template_name = 'clienttemplates/clientcontact.html'
    form_class = ContactForm
    success_url = '/'

    def form_valid(self, form):
        to_mail = form.cleaned_data['email']
        subject = form.cleaned_data['subject']
        from_mail = settings.EMAIL_HOST_USER
        body = "thanks for contacting us we will contact you shortly"
        send_mail(
            subject,
            body,
            from_mail,
            [to_mail],
            fail_silently=True)

        return super().form_valid(form)


class ClientAboutView(TemplateView):
    template_name = 'clienttemplates/clientabout.html'


class ClientSunilView(TemplateView):
    template_name = 'clienttemplates/clientsunil.html'


class ClientDipakView(TemplateView):
    template_name = 'clienttemplates/clientdipak.html'


class ClientCosmosView(TemplateView):
    template_name = 'clienttemplates/clientcosmos.html'


class ClientNoticeListView(ListView):
    template_name = 'clienttemplates/clientnoticelist.html'
    model = Notice
    context_object_name = 'clienttemplates/clientnoticelist'


class ClientNoticeDetailView(DetailView):
    template_name = 'clienttemplates/clientnoticedetail.html'
    model = Notice
    context_object_name = 'notice'
